#include <stdio.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void test_executor();
void* init();
void test_1(void* heap);
void test_2(void* heap);
void test_3(void* heap);