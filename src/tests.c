#include "tests.h"

void* init() {
    printf("0. Heap Initialization \n");

    void* heap = heap_init(1);

    debug_heap(stdout, heap);

    printf("\n\n");
    return heap;
}

void test_1(void* heap) {
    printf("1. Simple Allocation Test\n");

    void* malloc_mem_1 = _malloc(512);

    debug_heap(stdout, heap);
    _free(malloc_mem_1);
    printf("*freeing allocated memory*\n");
    debug_heap(stdout, heap);

    printf("\n\n");
}


void test_2(void* heap) {
    printf("2. Freeing Many Blocks \n");
    void* malloc_mem_1 = _malloc(1024);
    void* malloc_mem_2 = _malloc(2048);
    void* malloc_mem_3 = _malloc(1); // should be cast to 24 bytes
    void* malloc_mem_4 = _malloc(512);
    debug_heap(stdout, heap);

    _free(malloc_mem_4);
    _free(malloc_mem_3);
    _free(malloc_mem_2);
    _free(malloc_mem_1);
    debug_heap(stdout, heap);
    printf("\n\n");
}

void test_3(void* heap) {
    printf("3. Heap Expansion \n");
    debug_heap(stdout, heap);

    void* malloc_mem_0 = _malloc(9000);
    debug_heap(stdout, heap);
    void* malloc_mem_1 = _malloc(32768);
    debug_heap(stdout, heap);

    _free(malloc_mem_1);
    _free(malloc_mem_0);
    debug_heap(stdout, heap);

    printf("\n\n");
}

void test_4(void* heap){
    printf("4. Heap Expansion (with free block remain)");
    debug_heap(stdout, heap);

    void* malloc_mem_1 = _malloc(1);

    debug_heap(stdout, heap);
    _free(malloc_mem_1);

    debug_heap(stdout, heap);
    printf("\n\n");
}
void test_executor(){
    printf("---Tester Initialized---\n\n");
    void* heap;
    heap = init();
    test_1(heap);
    test_2(heap);
    test_3(heap);
    test_4(heap);

    printf("---Tests Are Done---\n\n");
}
